# Escape Game Loading bar

## Dependencies

You need to install the node packages `express` and `socket.io`, which can be done like this:
```
npm install express socket.io
```

## User manual

To run:

```
node index.js
```

Use node >=10.0.0

To display the progress of the escape game, navigate to [http://localhost](http://localhost)

To display the control panel, navigate to [http://localhost/ctrl](http://localhost/ctrl)

Place voice samples in `static/messages/xxx.yyy` and then in `messages.json` replace `""` with `xxx.yyy`

The code displayed by the objectives can be changed in `code.txt`
