const duration = 45; // Duration of the escape game in minutes


var express = require("express");
var app = express();
var http = require('http').Server(app);
var io = require("socket.io")(http);
var fs = require("fs");

var PORT = 80;

var msgs = {};
var boules = [false, false, false];
var avol = 50;

var code = fs.readFileSync("code.txt").toString().split("").splice(0, 4);

fs.readFile("messages.json", 'utf8', function(err, buf) {
  if (err) throw err;
  msgs = JSON.parse(buf.toString());
});

app.use("/", express.static("static"));

app.get("/", function(req, res){
  res.sendFile(__dirname + "/html/index.html");
});

app.get("/ctrl", function(req, res){
  res.sendFile(__dirname + "/html/controler.html");
});

io.on('connection', function(socket){
  console.log('an user connected');

  socket.emit('msgs', msgs);
  socket.emit('boules', boules, code);
  socket.emit('vol', avol);

  socket.on('say', function(msg){
    if (msgs[msg] == undefined){
      io.emit('send', msg);
      io.emit('bell');
    } else {
      io.emit('tell', msg, msgs[msg]);
    }
  });

  socket.on('boule', function(id){
    boules[id - 1] = !boules[id - 1];
    io.emit('boules', boules, code);
  });

  socket.on('start', function(){
    boules = [false, false, false];
    io.emit('boules', boules);
    stts = Date.now();
    it = setInterval(updateTimer, 100);
    io.emit('started');
  });

  socket.on('stop', function(){
    try { clearInterval(it); } catch (e) {}
    io.emit('stopped');
  });

  socket.on('ring', function(){
    io.emit('bell');
  });

  socket.on('clear', function(){
    io.emit('clr');
  });

  socket.on('setvol', function(vol){
    avol = vol;
    socket.broadcast.emit('vol', avol);
  })

  socket.on('rst', function(){
    boules = [false, false, false];
    io.emit('boules', boules);
  });
});

function updateTimer(){
  let min = Math.max(duration - (Date.now() - stts)/1000/60, 0);
  let sec = (min - Math.floor(min))*60;
  io.emit('time', min, sec);
}

http.listen(PORT, function(){
  console.log("Listening on port", PORT);
});
